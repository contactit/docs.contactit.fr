---
description: Bienvenue sur mon journal de bord.
date: 2022-09-09
visibility: public
order: 100
---

# :rocket: Bienvenue sur mon journal de bord 😃

=== :icon-chevron-right: Qui es-tu ?

:icon-accessibility: Passionné d'informatique depuis 9 ans maintenant 🧐, je suis une alternance pour préparer le titre professionnel d'administrateur d'infrastructures sécurisées (AIS) de niveau bac +4 avec l'école 2itech academy (M2I).  

Et pourquoi pas continuer sur un mastère 2 après 😉.  

=== :icon-chevron-right: Qu'est-ce que docs.contactit.fr ?

:icon-note: Un mémo des formations que j'ai suivi, des conseils d'intervenants, des éxperiences théoriques et pratiques, c'est un peux de tout finalement. 
Je me suis dit pourquoi pas le mettre en ligne et partager ces connaissances avec tous.

=== :icon-chevron-right: Pourquoi docs.contactit.fr ?

:icon-light-bulb: J'ai eu l'idée de créer un site "vitrine" principalement déstiné aux entreprises.  

:icon-pencil: J'aime rédiger, mettre à plat ce que j'ai appris et transmettre. Beaucoup de documentations sont disponible sur internet, mais elles sont souvent confuses et plus à jour.  

:icon-check: J'ai donc décidé de créer un sous-domaine `docs` pour y écrire des documentations agrémentées de petites tips à droite à gauche.

=== :icon-chevron-right: Comment as-tu construit docs.contactit.fr ?

:icon-arrow-right: Avec le projet **FOSS**: **retype** ! Un excellent **générateur de fichiers statiques** pour des présentations et documentations.  

:icon-comment-discussion: Les développeurs sont très actifs et répondent aux feedbacks et aux suggestions de la communauté (https://github.com/retypeapp/retype/issues).  

:icon-info: Les fichiers de configurations utilisent la syntaxe **Yaml** et pour le contenu **Markdown**.  

:icon-arrow-right: Le projet est disponible sur github à cette adresse: https://github.com/retypeapp/retype  
:icon-arrow-right: La documentation sur le site: https://retype.com/

:icon-code-of-conduct: L'intégralité de `docs.contactit.fr` est **libre** (licence MIT) et disponible sur **gitlab** à cette adresse: https://gitlab.com/contactit/docs.contactit.fr

=== :icon-chevron-right: Intéressant, je peux t'écrire un mot ?

:icon-arrow-right: Bien sur 😃 ! Je suis joignable par [mail](mailto:contactit.yarka@slmail.me).

===