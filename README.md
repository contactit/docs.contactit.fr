# 🚀 Bienvenue sur mon journal de bord 😃

---

## 🤔 Qui es-tu ?

Passionné d'informatique depuis 9 ans maintenant 🧐, je suis une alternance pour préparer le titre professionnel d'administrateur d'infrastructures sécurisées (AIS) de niveau bac +4 avec l'école 2itech academy (M2I).
Et pourquoi pas continuer sur un mastère 2 après 😉.

---

## 🤔 Qu'est-ce que docs.contactit.fr ?

📖 Un mémo des formations que j'ai suivi, des conseils d'intervenants, des éxperiences théoriques et pratiques, c'est un peux de tout finalement.  

🆙 Je me suis dit pourquoi pas le mettre en ligne et partager ces connaissances avec tous.

---

## 🤔 Pourquoi docs.contactit.fr ?

💡 J'ai eu l'idée de créer un site "vitrine" principalement déstiné aux entreprises.  

✏️ J'aime rédiger, mettre à plat ce que j'ai appris et transmettre. Beaucoup de documentations sont disponible sur internet, mais elles sont souvent confuses et plus à jour.  

📝 J'ai donc décidé de créer un sous-domaine docs pour y écrire des documentations agrémentées de petites tips à droite à gauche.

---

## 🤔 Comment as-tu construit docs.contactit.fr ?

🙃 Avec le projet FOSS: retype ! Un excellent générateur de fichiers statiques pour des présentations et documentations.  

👉 Les développeurs sont très actifs et répondent aux feedbacks et aux suggestions de la communauté (https://github.com/retypeapp/retype/issues).  
👉 Les fichiers de configurations utilisent la syntaxe Yaml et pour le contenu Markdown.  
👉 Le projet est disponible sur github à cette adresse: https://github.com/retypeapp/retype  
👉 La documentation sur le site: https://retype.com/  

🤜 L'intégralité de docs.contactit.fr est libre (licence MIT) et disponible sur gitlab à cette adresse: https://gitlab.com/contactit/docs.contactit.fr

---

## 🧐 Intéressant, je peux t'écrire un mot ?

Bien sur 😃 ! Je suis joignable par [mail](mailto:contactit.yarka@slmail.me)
